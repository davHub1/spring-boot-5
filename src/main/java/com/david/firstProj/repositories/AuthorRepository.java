package com.david.firstProj.repositories;

import com.david.firstProj.domain.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
